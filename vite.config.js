import {defineConfig} from "vite";
import react from "@vitejs/plugin-react";
import tsConfigPaths from "vite-tsconfig-paths";

export default defineConfig({
    server: {
        open: true,
        port: 2189
    },
    plugins: [
        react(),
        tsConfigPaths()
    ]
})