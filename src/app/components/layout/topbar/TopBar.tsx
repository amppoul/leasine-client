import React from 'react'
import FullScreen from "@leasine/components/layout/topbar/internal/FullScreen";
import Profile from "@leasine/components/layout/topbar/internal/Profile";
import Banner from "@leasine/components/banner/Banner";

type TopBarLeftProps = {
    toggleMenu: () => void
}

const TopBarRight = () => {
    return (<ul className="list-unstyled topnav-menu float-end mb-0">
        <FullScreen/>
        <Profile/>
    </ul>);
}

const TopBarLeft = ({ toggleMenu }: TopBarLeftProps) => {
    return (<ul className="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button
                className="button-menu-mobile waves-effect waves-light"
                onClick={toggleMenu}>
                <i className="ri-menu-line"></i>
            </button>
        </li>
    </ul>);
}

export default function TopBar() {

    const toggleMenu = () => console.log("toggle menu");

    return (
        <div className="navbar-custom">
            <div className="container-fluid">
                <TopBarRight/>
                <Banner/>
                <TopBarLeft toggleMenu={toggleMenu} />
                <div className="clearfix"></div>
            </div>
        </div>
    )
}