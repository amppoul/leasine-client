import React from 'react'
import avatar from '@leasine/assets/images/users/avatar-1.jpg'
import {Link} from "react-router-dom";

export default function Profile() {
    return (<li className="nav-item dropdown notification-list topbar-dropdown">
        <span
           className="nav-link dropdown-toggle p-0"
           role="button"
           data-bs-toggle="dropdown"
           aria-expanded="false">

            <div className="nav-user mr-0">
                <img src={avatar}
                     alt="avatar"
                     className="rounded-circle"
                />
                <span className="pro-user-name ml-1 align-middle">
                    {' Admin '}
                    <i className="ri-arrow-down-s-line align-middle"></i>
              </span>
            </div>
        </span>
        <ul className="dropdown-menu dropdown-menu-end profile-dropdown">
            <li>
                <Link to="#" className="dropdown-item">
                    <i className="ri-profile-line"></i>
                    <span>Profile</span>
                </Link>
            </li>
            <li>
                <Link to="#" className="dropdown-item">
                    <i className="ri-settings-3-line"></i>
                    <span>Settings</span>
                </Link>
            </li>

            <li>
                <hr className="dropdown-divider"/>
            </li>
            <Link to="#" className="dropdown-item">
                <i className="ri-logout-box-line"></i>
                <span>Logout</span>
            </Link>
        </ul>
    </li>)
}