import React from 'react';
import {Link} from "react-router-dom";

export default function FullScreen() {

    const toggleFullScreen = () => console.log("toggle fullscreen");

    return (<li className="dropdown d-none d-lg-inline-block">
            <Link to="#" className="nav-link dropdown-toggle arrow-none waves-effect waves-light"
                  onClick={toggleFullScreen}
                  data-toggle="fullscreen">
                <i className="ri-fullscreen-fill noti-icon"></i>
            </Link>
        </li>);
};