import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <div className="container-fluid">
        <div className="row">
          <div className="col">{ new Date().getFullYear() } &copy; Leasine</div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
