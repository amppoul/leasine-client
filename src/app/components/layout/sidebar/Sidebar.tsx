import React from 'react'
import SimpleBar from "simplebar-react";
import {Link} from "react-router-dom";
import {v4 as uuidv4} from "uuid";
import {navigation} from "@leasine/helpers/navigation-enum";
import Banner from "@leasine/components/banner/Banner";

type MenuItem = {
    id: string;
    label: string;
    icon?: string;
    path: string;
}

type MenuItemProps = {
    item: MenuItem
}

const items: MenuItem[] = [
    {
        id: uuidv4(),
        label: "Dashboard",
        icon: "ri-dashboard-line",
        path: navigation.HOME
    },
    {
        id: uuidv4(),
        label: "Vehicles",
        icon: "ri-car-line",
        path: navigation.VEHICLES
    },
    {
        id: uuidv4(),
        label: "Import",
        icon: "ri-upload-2-line",
        path: navigation.IMPORT
    },
    {
        id: uuidv4(),
        label: "Taxation",
        icon: "ri-funds-line",
        path: navigation.TAXATION
    }];

const SideBarItem = ({ item }: MenuItemProps) => {
    return (<li>
        <Link to={item.path} className="side-nav-ref-link">
            {item?.icon && <i className={`${item?.icon}`}></i>}
            <span>{`${item.label}`}</span>
        </Link>
    </li>);
};

export default function Sidebar() {
    return (<div className="left-side-menu">
        <Banner/>
        <SimpleBar className="h-100" data-simplebar>
            <div id="sidebar-menu">
                <ul id="side-menu" className="list-unstyled">
                    {items.map(item => <SideBarItem key={item.id} item={{...item}} />)}
                </ul>
            </div>
        </SimpleBar>
    </div>)
}
