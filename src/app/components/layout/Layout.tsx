import React from "react";
import Sidebar from "./sidebar/Sidebar";
import TopBar from "./topbar/TopBar";
import Footer from "./Footer";
import {Outlet} from "react-router-dom";

export default function Layout() {
    return (<div id="wrapper">
            <Sidebar/>
            <div className="content-page">
                <div className="content">
                    <TopBar/>
                    <div className="container-fluid">
                        <Outlet />
                    </div>
                </div>
                <Footer/>
            </div>
        </div>)
}