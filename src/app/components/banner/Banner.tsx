import React from 'react'
import {Link} from 'react-router-dom';
import {navigation} from "@leasine/helpers/navigation-enum";

export default function Banner() {
    return (<div className="logo-box">
        <Link to={navigation.HOME} className="logo logo-light text-center">
            <span className="logo-sm">
                <span className="logo-sm-text-light text-white">
                    <i className="ri-roadster-line align-middle"></i>
                </span>
            </span>
            <span className="logo-lg">
                <span className="logo-lg-text-light text-white">
                    <i className="ri-roadster-line align-middle"></i>
                    <span className="align-middle px-2">Leasine</span>
            </span>
          </span>
        </Link>
    </div>)
}