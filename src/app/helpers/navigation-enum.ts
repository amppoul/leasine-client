export enum navigation {
    HOME = "/",
    VEHICLES = "/vehicles",
    IMPORT = "/import",
    TAXATION = "/taxation"
}