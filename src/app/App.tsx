import React from "react";
import {Route, Routes} from "react-router-dom";
import appRoutes from "./routes";

function App() {
    return (<Routes>
        {
            appRoutes.map(parent =>
                (<Route key={parent?.id} path={parent.path} element={parent.element}>
                    {
                        parent?.children.map(child =>
                            (<Route key={child?.id}
                                    index={child?.index}
                                    path={child.path}
                                    element={child.element}/>
                            ))
                    }
                </Route>)
            )
        }
    </Routes>)
}

export default App;
