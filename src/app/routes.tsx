import Dashboard from "./pages/Dashboard";
import Vehicle from "./pages/Vehicle";
import Import from "./pages/Import";
import Taxation from "./pages/Taxation";
import {v4 as uuidv4} from "uuid";
import Layout from "./components/layout/Layout";
import {navigation} from "./helpers/navigation-enum";

const appRoutes = [
    {
        element: <Layout/>,
        path: navigation.HOME,
        id: uuidv4(),
        children: [
            {
                index: true,
                element: <Dashboard/>,
                id: uuidv4(),
            },
            {
                element: <Vehicle/>,
                id: uuidv4(),
                path: navigation.VEHICLES,
            },
            {
                element: <Import/>,
                id: uuidv4(),
                path: navigation.IMPORT,
            },
            {
                element: <Taxation/>,
                id: uuidv4(),
                path: navigation.TAXATION,
            }
        ],
    },
]
export default appRoutes