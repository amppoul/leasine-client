import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter} from 'react-router-dom'
import 'remixicon/fonts/remixicon.css'
import 'simplebar-react/dist/simplebar.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min'
import './index.scss';
import App from './app/App';

const root = document.getElementById('root') as HTMLElement;
ReactDOM.createRoot(root)
    .render(
        <BrowserRouter>
            <React.StrictMode>
                <App/>
            </React.StrictMode>
        </BrowserRouter>
    );
